################################################################################
#### IQTD -- Incremental Query Transition Diagram
################################################################################

Revisions:
	1: IQ_TD.pdf  (8/6/2014)
	2: IQ_TD2.pdf  (8/13/2014)
	3: IQ_TD3.pdf  (8/14/2014)
	4: IQ_TD4.pdf  (8/14/2014)
	5: IQ_TD5.pdf (8/15/2014)
	6: IQ_TD6.pdf (11/20/2014)  On Order of Paths.
	7: IQ_TD7.pdf (12/3/2014) Lexicographical Ordering On Paths used from order of rules in program.  Changes Highlighted.  
	8: IQ_TD8.pdf (12/11/2014) Changes to order, formal concept of positions in sequence related to inc(Q), highlighted. 
	9: IQ_TD9.pdf (12/13/2014) Integrates changes discussed in IQ_TD8_discussion.pdf
	10: IQ_TD10.pdf (1/13/2015) Integrates changes discussed in IQ_TD9_discussion.pdf
	11: IQ_TD11.pdf (1/15/2015) Integrates discussion over IQ_TD10.pdf
	12: IQ_TD12.pdf (1/16/2015) No review needed, all changes were minor and approved.  Tentative final draft. 
	13: IQ_TD13.pdf (1/22/2015) <S, dec(), S> is a command transition when S is the initial IQ state. 
	14: IQ_TD14.pdf (1/04/2015) Revised definition of the incremental query command related to IQ command.  
	
	
Proof Of Correctness: 
	1: IQ_TD_Correctness1.pdf (1/13/2015)   Beginning of structure of proof.  
	2: IQ_TD_Correctness2.pdf (1/15/2015)   Beginning of structure of proof.  
	3: IQ_TD_Correctness3.pdf (1/20/2015)   Revised from discussion, right to left, includes 4 soundness theorem statements. 
	4: IQ_TD_Correctness4.pdf (1/22/2015)   Revised lemmas
	5. IQ_TD_Correctness5.pdf (1/26/2015)   Includes discussed changes, added intuition on lemmas. 
	6. IQ_TD_Correctness6.pdf (2/04/2015)	Revised proof of Solution Request Lemma. 
	
The incremental CLP Transition Diagram  
	Overall: IQ_TD.pdf    (8/6/2014)(8/3/2014) (7/31/2014) (7/28/2014)
		OD: active\Edward\fromEdward\Documents\WorkingDirectory
	Proof:IQ_TD_Correctness.pdf (7/31/2014) (7/28/2014)
	
Slides for iCLP Transition Diagram. 
	File: IQ_TD_Slides.pdf

Example PICTURE of Abstract IQ Derivation Tree. 
	File: IQ_Deriv_Tree_Picture.pdf

IQ Algorithm written by Dr. Gelfond
	File: IncQueryAlgorithm.pdf

Incremental WAM instruction modification
	File: incrementalWAM.pdf
